#!/bin/bash

: << __NOTA__

    Para cambiar la configuracion del software, modificar directamente
    el json dentro de _DIRNAME/config/, parar y eliminar el software instalado 
    y volver a correr este script o rogar para que el sofware lo detecte 
    automaticamente.

__NOTA__

trap clean 1 2 15

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

#[START] installation variables
export _INSTALLATON_FOLDER=/var/gnome_session_daemd
export _SCRIPT_NAME="$( basename "$0" )"
export _DIRNAME="$( readlink -f $( dirname "$0" ) )"
export _CURRENT_SCRIPT_PATH="$( readlink -f "$_DIRNAME/$_SCRIPT_NAME" )"
export _CONFIG_FOLDER="$_DIRNAME/config"
export _CONFIG_JSON="$_CONFIG_FOLDER/config.conf"
#[END] installation variables

#[START] docker variables
export _DOCKERFILE_NAME="dfile_min1"
export _DOCKER_IMAGE_NAME="google_chd"
export _DOCKER_CONTAINER_NAME="google_chd"
export _DOCKER_CREATE_OPT="pull"  #  [build|pull]
export _DOCKER_CONTAINER_REGISTRY="registry.gitlab.com/mrc_es/google_daed"
#[END] docker variables

#[START] log variables
export _CONF_STATUS_FILENAME="status.conf"
export _CONF_STATUS_FILE="$_INSTALLATON_FOLDER/$_CONF_STATUS_FILENAME"
export _CUR_STATUS=OK
#[END] log variables

set_status() {
    declare _DATE="$( date +'%Y/%m/%dT%H:%M:%S' )"

    [[ -s "$_CONF_STATUS_FILE" ]] \
        || printf 'last ok: \nlast fail: \n' > "$_CONF_STATUS_FILE"

    [[ "$_CUR_STATUS" = "OK" ]] \
        && sed -ri "s@(last ok:)(.*)@\1$_DATE@g" "$_CONF_STATUS_FILE" \
        || sed -ri "s@(last fail:)(.*)@\1$_DATE@g" "$_CONF_STATUS_FILE"
}

docker_build_opts() {

    if [[ "$_DOCKER_CREATE_OPT" = "build" ]]
    then
        docker build \
            --file="$_INSTALLATON_FOLDER/$_DOCKERFILE_NAME" \
            -t "$_DOCKER_IMAGE_NAME" \
            "$_INSTALLATON_FOLDER"
    elif [[ "$_DOCKER_CREATE_OPT" = "pull" ]]
    then
        docker pull "$_DOCKER_CONTAINER_REGISTRY"
        docker tag "$_DOCKER_CONTAINER_REGISTRY" "$_DOCKER_IMAGE_NAME"
        docker image rm "$_DOCKER_CONTAINER_REGISTRY"
    fi
}

docker_setup() {
    apt list --installed 2> /dev/null | egrep -q "^docker"
    [[ ${PIPESTATUS[1]} -ne 0 ]] \
        && curl -fsSL https://get.docker.com | bash

    docker image ls -a | awk '{printf "%s:%s\n",$1,$2}' | grep -q "$_DOCKER_IMAGE_NAME"
    [[ ${PIPESTATUS[2]} -ne 0 ]] \
        && docker_build_opts

    declare -l _CONTAINER_STATUS="$(
        docker ps \
            -a \
            --filter name="$_DOCKER_CONTAINER_NAME" \
            --format '{{.Status}}' \
        2> /dev/null | egrep -m1 -o "Up|Exited|Created"
    )"

    if [[ ! "$_CONTAINER_STATUS" ]]
    then

        docker run \
            -d \
            --name $_DOCKER_CONTAINER_NAME \
            -v "$_CONFIG_FOLDER":/usr/bin/gnome_session/config/ \
            "$_DOCKER_IMAGE_NAME"

    elif [[ "$_CONTAINER_STATUS" && "$_CONTAINER_STATUS" != "up" ]]
    then

        docker container rm "$_DOCKER_CONTAINER_NAME"
        docker run \
            -d \
            --name $_DOCKER_CONTAINER_NAME \
            -v "$_CONFIG_FOLDER":/usr/bin/gnome_session/config/ \
            "$_DOCKER_IMAGE_NAME"

    fi
}

auto_install() {
    [[ -d "$_INSTALLATON_FOLDER" ]] \
        || { mkdir "$_INSTALLATON_FOLDER" || _CUR_STATUS=ERROR; }
    [[ -e "$_INSTALLATON_FOLDER/$_SCRIPT_NAME" ]] \
        || { cp -rT "$_DIRNAME" "$_INSTALLATON_FOLDER" || _CUR_STATUS=ERROR; }
    [[ -x "$_INSTALLATON_FOLDER/$_SCRIPT_NAME" ]] \
        || { chmod +x "$_INSTALLATON_FOLDER/$_SCRIPT_NAME" || _CUR_STATUS=ERROR; }

    set_status
}

cron_config() {
    declare _COMMAND="bash $_INSTALLATON_FOLDER/$_SCRIPT_NAME -y"
    declare _PERIODICITY="*/30 * * * *"
    declare _CRONTAB_STRING="$_PERIODICITY $_COMMAND"

    ( crontab -l; echo "$_CRONTAB_STRING"; echo "" ) | sort | uniq | crontab -
}

clean() {
    [[ $_CUR_STATUS != "ERROR" && "$_DIRNAME" != "$_INSTALLATON_FOLDER" ]] \
        && rm -rf "$_DIRNAME"
}

suggest_edit() {

    echo "Sugerencia: "
    echo 
    echo "Para cambiar la configuracion del software, modificar directamente"
    echo "el json de configuracion, parar y eliminar el software instalado y"
    echo "volver a correr este script."
    echo -n "Desea continuar? (y/n): "
    
    while :
    do
        read respuesta

        if [[ "$respuesta" =~ ^(n|N)$ ]]
        then
            exit 1
        elif [[ "$respuesta" =~ ^(y|Y)$ ]]
        then
            return
        else
            echo -n "Conteste 'y' o 'n': "
        fi
    done

}

main() {
    
    [[ "$1" != "-y" ]] && suggest_edit 
    auto_install
    docker_setup
    cron_config
    clean

}

main $@

